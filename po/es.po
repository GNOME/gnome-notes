# Spanish translation for bijiben.
# Copyright (C) 2012 bijiben's COPYRIGHT HOLDER
# This file is distributed under the same license as the bijiben package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Daniel Mustieles <daniel.mustieles@gmail.com>, 2012-2023.
# Daniel Mustieles García <daniel.mustieles@gmail.com>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: bijiben master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-notes/issues\n"
"POT-Creation-Date: 2023-04-28 08:20+0000\n"
"PO-Revision-Date: 2023-07-19 15:59+0200\n"
"Last-Translator: Daniel Mustieles García <daniel.mustieles@gmail.com>\n"
"Language-Team: Spanish - Spain <gnome-es-list@gnome.org>\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 42.0\n"

#: data/appdata/org.gnome.Notes.appdata.xml.in:6
#: data/org.gnome.Notes.desktop.in:3 data/org.gnome.Notes.xml.in:4
#: src/bjb-application.c:402 src/bjb-window.c:46
msgid "Notes"
msgstr "Notas"

#: data/appdata/org.gnome.Notes.appdata.xml.in:7
msgid "Notes for GNOME"
msgstr "Notas para GNOME"

#: data/appdata/org.gnome.Notes.appdata.xml.in:9
msgid ""
"A quick and easy way to make freeform notes or jot down simple lists. Store "
"as many notes as you like and share them by email."
msgstr ""
"Una manera rápida y fácil de crear notas o listas sencillas. Guarde todas "
"las notas que quiera y compártalas por correo-e."

#: data/appdata/org.gnome.Notes.appdata.xml.in:12
msgid ""
"You can store your notes locally on your computer or sync with online "
"services like ownCloud."
msgstr ""
"Puede guardar las notas en su equipo o sincronizarlas con servicios en línea "
"como ownCloud."

#: data/appdata/org.gnome.Notes.appdata.xml.in:28
msgid "Edit view"
msgstr "Vista de edición"

#: data/appdata/org.gnome.Notes.appdata.xml.in:32
msgid "Select view"
msgstr "Vista de selección"

#: data/appdata/org.gnome.Notes.appdata.xml.in:36
msgid "List view"
msgstr "Vista de lista"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Notes.desktop.in:5
msgid "notes;reminder;notebook;sticky notes;"
msgstr "notas;recordatorio;cuaderno;adhesivas;"

#: data/org.gnome.Notes.desktop.in:6
msgid "Post notes, tag files!"
msgstr "Publique notas y etiquete archivos"

#: data/org.gnome.Notes.desktop.in:7
msgid "Note-taker"
msgstr "Para tomar notas"

#: data/org.gnome.Notes.desktop.in:19
msgid "Create New Note"
msgstr "Crear una nota nueva"

#: data/org.gnome.Notes.gschema.xml:10
msgid "Custom Font"
msgstr "Tipografía personalizada"

#: data/org.gnome.Notes.gschema.xml:11
msgid "The font name set here will be used as the font when displaying notes."
msgstr ""
"El nombre de la tipografía aquí establecido se usará como tipografía al "
"mostrar las notas."

#: data/org.gnome.Notes.gschema.xml:15
msgid "Whether to use the system monospace font"
msgstr "Indica si se debe usar la tipografía monoespaciada del sistema"

#: data/org.gnome.Notes.gschema.xml:19
msgid "New notes color."
msgstr "Nuevo color de las notas."

#: data/org.gnome.Notes.gschema.xml:20
msgid ""
"The color name set here will be used as the color when creating new notes."
msgstr ""
"El nombre del color aquí establecido se usará como el color al crear las "
"notas nuevas."

#: data/org.gnome.Notes.gschema.xml:24
msgid "Primary notes provider to use for new notes."
msgstr "Proveedor de notas primario que usar para las notas nuevas."

#: data/org.gnome.Notes.gschema.xml:25
msgid "The primary notebook is the place where new notes are created."
msgstr ""
"El cuaderno de notas primario es el lugar donde se crean las notas nuevas."

#: data/org.gnome.Notes.gschema.xml:29
msgid "Note path of the notes opened when Notes quits last time."
msgstr ""
"Nota con la ruta de las notas abiertas cuando se cerró Notas por última vez."

#: data/org.gnome.Notes.gschema.xml:30
msgid ""
"This stores the note path of the last opened item, so that we can open it on "
"startup."
msgstr ""
"Esto guarda la ruta de la nota del último elemento abierto, por lo que se "
"puede abrir al iniciar."

#: data/org.gnome.Notes.gschema.xml:34
msgid "Window maximized"
msgstr "Ventana maximizada"

#: data/org.gnome.Notes.gschema.xml:35
msgid "Window maximized state."
msgstr "Estado maximizado de la ventana."

#: data/org.gnome.Notes.gschema.xml:39
msgid "Window size"
msgstr "Tamaño de la ventana"

#: data/org.gnome.Notes.gschema.xml:40
msgid "Window size (width and height)."
msgstr "Tamaño de la ventana (anchura y altura)."

#: data/org.gnome.Notes.gschema.xml:44
msgid "Window position"
msgstr "Posición de la ventana"

#: data/org.gnome.Notes.gschema.xml:45
msgid "Window position (x and y)."
msgstr "Posición de la ventana (X e Y)."

#: data/org.gnome.Notes.gschema.xml:48
msgid "Text size used by note editor."
msgstr "Tamaño de texto usado por el editor de notas."

#: data/org.gnome.Notes.gschema.xml:49
msgid ""
"There are three text sizes available: small, medium (default) and large."
msgstr ""
"Hay tres tamaños de texto disponibles: pequeña, mediana (predeterminado) y "
"grande."

#: data/resources/bjb-notebooks-dialog.ui:5 data/resources/bjb-window.ui:243
#: data/resources/bjb-window.ui:334 data/resources/selection-toolbar.ui:34
msgid "Notebooks"
msgstr "Cuadernos de notas"

#: data/resources/bjb-notebooks-dialog.ui:30
msgid "New Notebook…"
msgstr "Cuaderno de notas nuevo…"

#: data/resources/bjb-notebooks-dialog.ui:37
msgid "Add"
msgstr "Añadir"

#: data/resources/bjb-note-view.ui:15 src/bjb-note-view.c:338
msgid "No note selected"
msgstr "Ninguna nota seleccionada"

#. bjb_controller_set_group (self->controller, BIJI_LIVING_ITEMS);
#: data/resources/bjb-window.ui:29 data/resources/bjb-window.ui:224
#: src/bjb-window.c:303
msgid "All Notes"
msgstr "Todas las notas"

#: data/resources/bjb-window.ui:43
msgid "Open menu"
msgstr "Abrir el menú"

#: data/resources/bjb-window.ui:89
msgid "More options"
msgstr "Más opciones"

#: data/resources/bjb-window.ui:121 data/resources/import-dialog.ui:5
msgid "Import Notes"
msgstr "Importar notas"

#: data/resources/bjb-window.ui:136
msgid "Text Sizes"
msgstr "Tamaños del texto"

#: data/resources/bjb-window.ui:145
msgid "_Large"
msgstr "_Grande"

#: data/resources/bjb-window.ui:154
msgid "_Medium"
msgstr "_Mediano"

#: data/resources/bjb-window.ui:163
msgid "_Small"
msgstr "_Pequeño"

#: data/resources/bjb-window.ui:179 data/resources/settings-dialog.ui:7
msgid "Preferences"
msgstr "Preferencias"

#: data/resources/bjb-window.ui:187
msgid "Keyboard Shortcuts"
msgstr "Atajo del teclado"

#: data/resources/bjb-window.ui:195
msgid "Help"
msgstr "Ayuda"

#: data/resources/bjb-window.ui:203
msgid "About Notes"
msgstr "Acerca de notas"

#. bjb_controller_set_group (self->controller, BIJI_ARCHIVED_ITEMS);
#: data/resources/bjb-window.ui:268 src/bjb-window.c:313
msgid "Trash"
msgstr "Papelera"

#: data/resources/bjb-window.ui:295
msgid "Open in New Window"
msgstr "Abrir en una ventana nueva"

#: data/resources/bjb-window.ui:311
msgid "Undo"
msgstr "Deshacer"

#: data/resources/bjb-window.ui:319
msgid "Redo"
msgstr "Rehacer"

#: data/resources/bjb-window.ui:342
msgid "Email this Note"
msgstr "Enviar esta nota por correo-e"

#: data/resources/bjb-window.ui:350 data/resources/selection-toolbar.ui:96
msgid "Move to Trash"
msgstr "Mover a la papelera"

#: data/resources/bjb-window.ui:394
msgid "Select Note Location"
msgstr "Seleccionar la ubicación de la nota"

#: data/resources/editor-toolbar.ui:20
msgid "Bold"
msgstr "Negrita"

#: data/resources/editor-toolbar.ui:28
msgid "Italic"
msgstr "Cursiva"

#: data/resources/editor-toolbar.ui:36
msgid "Strike"
msgstr "Tachada"

#: data/resources/editor-toolbar.ui:52
msgid "Bullets"
msgstr "Topos"

#: data/resources/editor-toolbar.ui:60
msgid "List"
msgstr "Lista"

#: data/resources/editor-toolbar.ui:76
msgid "Indent"
msgstr "Sangrado"

#: data/resources/editor-toolbar.ui:84
msgid "Outdent"
msgstr "Sangrado (por la derecha)"

#: data/resources/editor-toolbar.ui:106
msgid "Copy selection to a new note"
msgstr "Copiar selección a una nota nueva"

#: data/resources/help-overlay.ui:9
msgctxt "shortcut window"
msgid "General"
msgstr "General"

#: data/resources/help-overlay.ui:14
msgctxt "shortcut window"
msgid "Search notes"
msgstr "Buscar notas"

#: data/resources/help-overlay.ui:21
msgctxt "shortcut window"
msgid "New note"
msgstr "Nota nueva"

#: data/resources/help-overlay.ui:28
msgctxt "shortcut window"
msgid "Close window"
msgstr "Cerrar la ventana"

#: data/resources/help-overlay.ui:35
msgctxt "shortcut window"
msgid "Quit"
msgstr "Salir"

#: data/resources/help-overlay.ui:42
msgctxt "shortcut window"
msgid "Go back"
msgstr "Retroceder"

#: data/resources/help-overlay.ui:49
msgctxt "shortcut window"
msgid "Show help"
msgstr "Mostrar ayuda"

#: data/resources/help-overlay.ui:56
msgctxt "shortcut window"
msgid "Open menu"
msgstr "Abrir menú"

#: data/resources/help-overlay.ui:63
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr "Atajos del teclado"

#: data/resources/help-overlay.ui:71
msgctxt "shortcut window"
msgid "Selection mode"
msgstr "Modo de selección"

#: data/resources/help-overlay.ui:76
msgctxt "shortcut window"
msgid "Cancel selection mode"
msgstr "Salir del modo de selección"

#: data/resources/help-overlay.ui:83
msgctxt "shortcut window"
msgid "Select all"
msgstr "Seleccionar todas"

#: data/resources/help-overlay.ui:91
msgctxt "shortcut window"
msgid "Note edit mode"
msgstr "Modo de edición de la nota"

#: data/resources/help-overlay.ui:96
msgctxt "shortcut window"
msgid "Open in a new window"
msgstr "Abrir en una ventana nueva"

#: data/resources/help-overlay.ui:103
msgctxt "shortcut window"
msgid "Bold"
msgstr "Negrita"

#: data/resources/help-overlay.ui:110
msgctxt "shortcut window"
msgid "Italic"
msgstr "Cursiva"

#: data/resources/help-overlay.ui:117
msgctxt "shortcut window"
msgid "Strike through"
msgstr "Tachado"

#: data/resources/help-overlay.ui:124
msgctxt "shortcut window"
msgid "Undo"
msgstr "Deshacer"

#: data/resources/help-overlay.ui:131
msgctxt "shortcut window"
msgid "Redo"
msgstr "Rehacer"

#: data/resources/help-overlay.ui:138
msgctxt "shortcut window"
msgid "Move note to trash"
msgstr "Mover nota a la papelera"

#: data/resources/import-dialog.ui:12
msgid "_Cancel"
msgstr "_Cancelar"

#: data/resources/import-dialog.ui:22
msgid "_Import"
msgstr "_Importar"

#: data/resources/import-dialog.ui:41
msgid "Select import location"
msgstr "Seleccionar la ubicación de la importación"

#: data/resources/import-dialog.ui:78
msgid "Gnote application"
msgstr "Aplicación Gnote"

#: data/resources/import-dialog.ui:136
msgid "Tomboy application"
msgstr "Aplicación Tomboy"

#: data/resources/import-dialog.ui:192
msgid "Custom Location"
msgstr "Ubicación personalizada"

#: data/resources/import-dialog.ui:224
msgid "Select a Folder"
msgstr "Seleccionar una carpeta"

#: data/resources/selection-toolbar.ui:43
msgid "Note color"
msgstr "Color de la nota"

#: data/resources/selection-toolbar.ui:60
msgid "Share note"
msgstr "Compartir nota"

#: data/resources/selection-toolbar.ui:78
msgid "Open in another window"
msgstr "Abrir en otra ventana"

#: data/resources/selection-toolbar.ui:119
msgid "Restore"
msgstr "Restaurar"

#: data/resources/selection-toolbar.ui:127
msgid "Permanently Delete"
msgstr "Eliminar permanentemente"

#: data/resources/settings-dialog.ui:23
msgid "Note Appearance"
msgstr "Apariencia de la nota"

#: data/resources/settings-dialog.ui:32
msgid "Use System Font"
msgstr "Usar la tipografía del sistema"

#: data/resources/settings-dialog.ui:43
msgid "Note Font"
msgstr "Tipografía de la nota"

#: data/resources/settings-dialog.ui:53
msgid "Default Color"
msgstr "Color predeterminado"

#: src/bijiben-shell-search-provider.c:225 src/bjb-window.c:110
msgid "Untitled"
msgstr "Sin título"

#: src/bjb-application.c:146
msgid "GNOME Notes"
msgstr "Notas de GNOME"

#: src/bjb-application.c:172
msgid "Show verbose logs"
msgstr "Mostrar registros detallados"

#: src/bjb-application.c:174
msgid "Show the application’s version"
msgstr "Mostrar la versión de la aplicación"

#: src/bjb-application.c:176
msgid "Create a new note"
msgstr "Crear una nota nueva"

#: src/bjb-application.c:184
msgid "[FILE…]"
msgstr "[ARCHIVO…]"

#: src/bjb-application.c:185
msgid "Take notes and export them everywhere."
msgstr "Tomar notas y exportarlas a cualquier parte."

#: src/bjb-application.c:403
msgid "Simple notebook for GNOME"
msgstr "Cuaderno de notas sencillo para GNOME"

#: src/bjb-application.c:409
msgid "translator-credits"
msgstr "Daniel Mustieles <daniel.mustieles@gmail.com>, 2012, 2013"

#: src/bjb-color-button.c:140
msgid "Note Color"
msgstr "Color de la nota"

#: src/bjb-note-list.c:113
msgid "Add Notes"
msgstr "Añadir notas"

#: src/bjb-note-list.c:114
msgid "Use the + button to add a note"
msgstr "Use el botón + para crear una nota"

#: src/bjb-note-list.c:119
msgid "No Results"
msgstr "No hay resultados"

#: src/bjb-note-list.c:120
msgid "Try a different search"
msgstr "Pruebe a hacer una búsqueda diferente"

#: src/bjb-note-view.c:146
msgid "This note is being viewed in another window"
msgstr "Esta nota se está viendo en otra ventana"

#: src/bjb-utils.c:51
msgid "Unknown"
msgstr "Desconocida"

#: src/bjb-utils.c:68
msgid "Yesterday"
msgstr "Ayer"

#: src/bjb-utils.c:74
msgid "This month"
msgstr "Este mes"

#. Translators: %s is the note last recency description.
#. * Last updated is placed as in left to right language
#. * right to left languages might move %s
#. *         '%s Last Updated'
#.
#: src/bjb-window.c:583
#, c-format
msgid "Last updated: %s"
msgstr "Última actualización %s"

#: src/providers/bjb-local-provider.c:78
msgid "Local"
msgstr "Local"

#: src/providers/bjb-local-provider.c:91
msgid "On This Computer"
msgstr "En este equipo"

#: src/providers/bjb-memo-provider.c:90
msgid "Memo Note"
msgstr "Nota de recordatorio"

#~ msgid "Select the default storage location:"
#~ msgstr "Seleccionar la ubicación predeterminada del almacenamiento:"

#~ msgid "Primary Book"
#~ msgstr "Libro principal"

#~ msgid "Local storage"
#~ msgstr "Almacenamiento local"

#~ msgid "No notes"
#~ msgstr "No hay notas"

#~ msgid "Notebook"
#~ msgstr "Cuaderno de notas"

#~ msgid "Oops"
#~ msgstr "Vaya"

#~ msgid "Please install “Tracker” then restart the application."
#~ msgstr "Instale «Tracker» y reinicie la aplicación."

#~ msgid "Enter a name to create a notebook"
#~ msgstr "Introduzca un nombre para crear un cuaderno de notas"

#~ msgid "Today"
#~ msgstr "Hoy"

#~ msgid "This week"
#~ msgstr "Esta semana"

#~ msgid "This year"
#~ msgstr "Este año"

#~ msgid "How to show note items"
#~ msgstr "Cómo mostrar elementos de notas"

#~ msgid "Whether to show note items in icon view or list view."
#~ msgstr "Indica si se deben mostrar las notas en vista de iconos o de lista."

#~ msgid "_New"
#~ msgstr "_Nueva"

#~ msgid "_Empty"
#~ msgstr "_Vaciar"

#~ msgid "Exit selection mode"
#~ msgstr "Salir del modo de selección"

#~ msgid "Search note titles, content and notebooks"
#~ msgstr "Buscar en títulos de notas, contenido y cuadernos de notas"

#~ msgid "View notes and notebooks in a grid"
#~ msgstr "Ver notas y cuadernos de notas en una rejilla"

#~ msgid "View notes and notebooks in a list"
#~ msgstr "Ver notas y cuadernos de notas en una lista"

#~ msgid "Selection mode"
#~ msgstr "Modo de selección"

#~ msgid "View Trash"
#~ msgstr "Ver la papelera"

#~ msgid "Could not parse arguments"
#~ msgstr "No se pudieron analizar los argumentos"

#~ msgid "Could not register the application"
#~ msgstr "No se pudo registrar la aplicación"

#~ msgid "Load More"
#~ msgstr "Cargar más"

#~ msgid "Loading…"
#~ msgstr "Cargando…"

#~ msgid "Click on items to select them"
#~ msgstr "Pulse sobre los elementos para seleccionarlos"

#~ msgid "Results for %s"
#~ msgstr "Resultados para %s"

#~ msgid "New and Recent"
#~ msgstr "Nuevas y recientes"

#~ msgid "* "
#~ msgstr "* "

#~ msgid "1."
#~ msgstr "1."

#~ msgctxt "shortcut window"
#~ msgid "Shortcuts"
#~ msgstr "Atajos"

#~| msgid "org.gnome.bijiben"
#~ msgid "org.gnome.Notes"
#~ msgstr "org.gnome.Notes"

#~ msgid "Bijiben"
#~ msgstr "Bijiben"

#~ msgid "Quickly jot notes"
#~ msgstr "Notas rápidas"

#~ msgid "Cut"
#~ msgstr "Cortar"

#~ msgid "Copy"
#~ msgstr "Copiar"

#~ msgid "Paste"
#~ msgstr "Pegar"

#~ msgid "About"
#~ msgstr "Acerca de"

#~ msgid "Notes is an editor allowing to make simple lists for later use."
#~ msgstr ""
#~ "Notas es un editor que permite hacer listas sencillas para usarlas más "
#~ "tarde."

#~ msgid "It allows to use ownCloud as a storage location and online editor."
#~ msgstr "Permite usar ownCloud como almacenamiento y editor en línea."

#~ msgid "_Import Notes"
#~ msgstr "_Importar notas"

#~ msgid "_Preferences"
#~ msgstr "Prefere_ncias"

#~ msgid "Note Edition"
#~ msgstr "Edición de la nota"

#~ msgid "Cancel"
#~ msgstr "Cancelar"

#~ msgid "Note"
#~ msgstr "Nota"

#~ msgid "notes;reminder;"
#~ msgstr "notas;recordatorio;"

#~ msgid "_OK"
#~ msgstr "_Aceptar"

#~ msgid "Numbered List"
#~ msgstr "Lista numerada"

#~ msgid "New"
#~ msgstr "Nuevo"

#~ msgid "No Notes Found"
#~ msgstr "No se han encontrado notas"

#~ msgid ""
#~ "Your notes notebook is empty.\n"
#~ "Click the New button to create your first note."
#~ msgstr ""
#~ "Su cuaderno de notas está vacía.\n"
#~ "Pulse el botón Nueva para crear su primera nota."

#~| msgid "_Close"
#~ msgid "Close"
#~ msgstr "Cerrar"

#~ msgid "_Close"
#~ msgstr "_Cerrar"

#~ msgid "No result found for this research."
#~ msgstr "No se han encontrado resultados para esta búsqueda."

#~ msgid "Delete"
#~ msgstr "Eliminar"

#~ msgid "Collections"
#~ msgstr "Colecciones"

#~ msgid "Collection"
#~ msgstr "Colección"

#~ msgid "New collection"
#~ msgstr "Colección nueva"

#~ msgid "Add to Collection"
#~ msgstr "Añadir a la colección"

#~ msgid "Rename"
#~ msgstr "Renombrar"

#~ msgid "Done"
#~ msgstr "Hecho"

#~ msgid "_About Bijiben"
#~ msgstr "Acerca _de Bijiben"

#~ msgid "Import notes from:"
#~ msgstr "Importar notas desde:"

#~ msgid "Tags"
#~ msgstr "Etiquetas"

#~ msgid "New tag"
#~ msgstr "Etiqueta nueva"

#~ msgid "External Notes"
#~ msgstr "Notas externas"

#~ msgid "Click on the external notes to import"
#~ msgstr "Pulse en las notas externas para importarlas"

#~ msgid "Choose a color for note"
#~ msgstr "Elija un color para la nota"

#~ msgid "Link"
#~ msgstr "Enlace"

#~ msgid "Tag"
#~ msgstr "Etiqueta"

#~ msgid "_External Notes"
#~ msgstr "Notas externas"
