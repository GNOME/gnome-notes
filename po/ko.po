# -*- compile-command: "ko-po-check ko.po" -*-
#
# Korean translation for bijiben.
# This file is distributed under the same license as the bijiben package.
#
# Changwoo Ryu <cwryu@debian.org>, 2013-2015.
# Hongmin Yang <hongminy@naver.com>, 2018.
# Seong-ho Cho <shcho@gnome.org>, 2018-2023.
#
# - 이 프로그램의 이름인 Bijiben는 "비지벤"이라고 음역.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-notes master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-notes/issues\n"
"POT-Creation-Date: 2023-01-28 12:47+0000\n"
"PO-Revision-Date: 2023-03-03 01:43+0900\n"
"Last-Translator: Seong-ho Cho <shcho@gnome.org>\n"
"Language-Team: Korean <gnome-kr@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 2.3.1\n"

#: data/appdata/org.gnome.Notes.appdata.xml.in:6
#: data/org.gnome.Notes.desktop.in:3 data/org.gnome.Notes.xml.in:4
#: src/bjb-application.c:589 src/bjb-window.c:45
msgid "Notes"
msgstr "메모"

#: data/appdata/org.gnome.Notes.appdata.xml.in:7
msgid "Notes for GNOME"
msgstr "그놈용 메모"

#: data/appdata/org.gnome.Notes.appdata.xml.in:9
msgid ""
"A quick and easy way to make freeform notes or jot down simple lists. Store "
"as many notes as you like and share them by email."
msgstr ""
"메모를 자유롭게 작성하고 간단한 목록을 적는 간편하고 쉬운 수단입니다. 원하는 "
"만큼 메모를 저장하고 전자메일로 공유하세요."

#: data/appdata/org.gnome.Notes.appdata.xml.in:12
msgid ""
"You can store your notes locally on your computer or sync with online "
"services like ownCloud."
msgstr ""
"컴퓨터에 메모를 저장하거나 오운클라우드 같은 온라인 서비스에 동기화할 수 있습"
"니다."

#: data/appdata/org.gnome.Notes.appdata.xml.in:28
msgid "Edit view"
msgstr "편집 보기"

#: data/appdata/org.gnome.Notes.appdata.xml.in:32
msgid "Select view"
msgstr "선택 보기"

#: data/appdata/org.gnome.Notes.appdata.xml.in:36
msgid "List view"
msgstr "목록 보기"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Notes.desktop.in:5
msgid "notes;reminder;notebook;sticky notes;"
msgstr "notes;메모;reminder;알림;notebook;기록장;메모책;sticky notes;끈적이;"

#: data/org.gnome.Notes.desktop.in:6
msgid "Post notes, tag files!"
msgstr "메모를 쓰고 파일을 태그합니다!"

#: data/org.gnome.Notes.desktop.in:7
msgid "Note-taker"
msgstr "메모장"

#: data/org.gnome.Notes.desktop.in:19
msgid "Create New Note"
msgstr "새 메모 만들기"

#: data/org.gnome.Notes.gschema.xml:10
msgid "Custom Font"
msgstr "사용자 설정 글꼴"

#: data/org.gnome.Notes.gschema.xml:11
msgid "The font name set here will be used as the font when displaying notes."
msgstr "여기서 설정한 글꼴 이름은 메모를 표시할 글꼴로 사용합니다."

#: data/org.gnome.Notes.gschema.xml:15
msgid "Whether to use the system monospace font"
msgstr "시스템 고정폭 글꼴을 사용할지 여부"

#: data/org.gnome.Notes.gschema.xml:19
msgid "New notes color."
msgstr "새 메모 색."

#: data/org.gnome.Notes.gschema.xml:20
msgid ""
"The color name set here will be used as the color when creating new notes."
msgstr "여기서 설정한 색 이름은 새 메모를 만들 때 색으로 사용합니다."

#: data/org.gnome.Notes.gschema.xml:24
msgid "Primary notes provider to use for new notes."
msgstr "새 메모에 사용할 주요 메모책."

#: data/org.gnome.Notes.gschema.xml:25
msgid "The primary notebook is the place where new notes are created."
msgstr "주요 메모책은 새 메모를 만드는 위치를 말합니다."

#: data/org.gnome.Notes.gschema.xml:29
msgid "Note path of the notes opened when Notes quits last time."
msgstr "메모 프로그램을 닫은 최근에 연 메모 경로."

#: data/org.gnome.Notes.gschema.xml:30
msgid ""
"This stores the note path of the last opened item, so that we can open it on "
"startup."
msgstr ""
"메모 프로그램을 시작할 때 최근에 연 항목을 다시 열 수 있게 메모 경로를 저장합"
"니다."

#: data/org.gnome.Notes.gschema.xml:34
msgid "Window maximized"
msgstr "창 최대화"

#: data/org.gnome.Notes.gschema.xml:35
msgid "Window maximized state."
msgstr "창 최대화 상태."

#: data/org.gnome.Notes.gschema.xml:39
msgid "Window size"
msgstr "창 크기"

#: data/org.gnome.Notes.gschema.xml:40
msgid "Window size (width and height)."
msgstr "창 크기(너비 및 높이)."

#: data/org.gnome.Notes.gschema.xml:44
msgid "Window position"
msgstr "창 위치"

#: data/org.gnome.Notes.gschema.xml:45
msgid "Window position (x and y)."
msgstr "창 위치(가로 및 세로)."

#: data/org.gnome.Notes.gschema.xml:48
msgid "Text size used by note editor."
msgstr "메모 편집기에서 사용하는 텍스트 크기입니다."

#: data/org.gnome.Notes.gschema.xml:49
msgid ""
"There are three text sizes available: small, medium (default) and large."
msgstr "텍스트 크기에는 작게, 중간(기본), 크게 세가지가 있습니다."

#: data/resources/bjb-notebooks-dialog.ui:6 data/resources/bjb-window.ui:298
#: data/resources/bjb-window.ui:386 data/resources/selection-toolbar.ui:34
msgid "Notebooks"
msgstr "메모책"

#: data/resources/bjb-notebooks-dialog.ui:33
msgid "New Notebook…"
msgstr "새 메모책…"

#: data/resources/bjb-notebooks-dialog.ui:43
msgid "Add"
msgstr "추가"

#: data/resources/bjb-note-view.ui:12 src/bjb-note-view.c:247
msgid "No note selected"
msgstr "선택한 메모 없음"

#: data/resources/bjb-window.ui:47 data/resources/bjb-window.ui:277
#: src/bjb-window.c:397
msgid "All Notes"
msgstr "모든 메모"

#: data/resources/bjb-window.ui:63
msgid "Open menu"
msgstr "메뉴 열기"

#: data/resources/bjb-window.ui:133
msgid "More options"
msgstr "추가 옵션"

#: data/resources/bjb-window.ui:173 data/resources/import-dialog.ui:5
msgid "Import Notes"
msgstr "메모 가져오기"

#: data/resources/bjb-window.ui:190
msgid "Text Sizes"
msgstr "텍스트 크기"

#: data/resources/bjb-window.ui:199
msgid "_Large"
msgstr "크게(_L)"

#: data/resources/bjb-window.ui:208
msgid "_Medium"
msgstr "중간(_M)"

#: data/resources/bjb-window.ui:217
msgid "_Small"
msgstr "작게(_S)"

#: data/resources/bjb-window.ui:234 data/resources/settings-dialog.ui:10
msgid "Preferences"
msgstr "기본 설정"

#: data/resources/bjb-window.ui:242
msgid "Keyboard Shortcuts"
msgstr "키보드 바로 가기"

#: data/resources/bjb-window.ui:250
msgid "Help"
msgstr "도움말"

#: data/resources/bjb-window.ui:258
msgid "About Notes"
msgstr "메모 정보"

#: data/resources/bjb-window.ui:324 src/bjb-window.c:406
msgid "Trash"
msgstr "휴지통"

#: data/resources/bjb-window.ui:346
msgid "Open in New Window"
msgstr "새 창에서 열기"

#: data/resources/bjb-window.ui:362
msgid "Undo"
msgstr "실행 취소"

#: data/resources/bjb-window.ui:370
msgid "Redo"
msgstr "다시 실행"

#: data/resources/bjb-window.ui:394
msgid "Email this Note"
msgstr "이 메모 메일로 보내기"

#: data/resources/bjb-window.ui:402 data/resources/selection-toolbar.ui:96
msgid "Move to Trash"
msgstr "휴지통으로 옮기기"

#: data/resources/editor-toolbar.ui:63
msgid "Bold"
msgstr "굵게"

#: data/resources/editor-toolbar.ui:79
msgid "Italic"
msgstr "기울임"

#: data/resources/editor-toolbar.ui:95
msgid "Strike"
msgstr "취소선"

#: data/resources/editor-toolbar.ui:123
msgid "Bullets"
msgstr "글 머리 기호"

#: data/resources/editor-toolbar.ui:139
msgid "List"
msgstr "목록"

#: data/resources/editor-toolbar.ui:167
msgid "Indent"
msgstr "들여쓰기"

#: data/resources/editor-toolbar.ui:183
msgid "Outdent"
msgstr "내어쓰기"

#: data/resources/editor-toolbar.ui:222
msgid "Copy selection to a new note"
msgstr "선택 항목을 새 메모로 복사합니다"

#: data/resources/help-overlay.ui:9
msgctxt "shortcut window"
msgid "General"
msgstr "일반"

#: data/resources/help-overlay.ui:14
msgctxt "shortcut window"
msgid "Search notes"
msgstr "메모 검색"

#: data/resources/help-overlay.ui:21
msgctxt "shortcut window"
msgid "New note"
msgstr "새 메모"

#: data/resources/help-overlay.ui:28
msgctxt "shortcut window"
msgid "Close window"
msgstr "창 닫기"

#: data/resources/help-overlay.ui:35
msgctxt "shortcut window"
msgid "Quit"
msgstr "끝내기"

#: data/resources/help-overlay.ui:42
msgctxt "shortcut window"
msgid "Go back"
msgstr "뒤로 가기"

#: data/resources/help-overlay.ui:49
msgctxt "shortcut window"
msgid "Show help"
msgstr "도움말 보기"

#: data/resources/help-overlay.ui:56
msgctxt "shortcut window"
msgid "Open menu"
msgstr "메뉴 열기"

#: data/resources/help-overlay.ui:63
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr "키보드 바로 가기"

#: data/resources/help-overlay.ui:71
msgctxt "shortcut window"
msgid "Selection mode"
msgstr "선택 모드"

#: data/resources/help-overlay.ui:76
msgctxt "shortcut window"
msgid "Cancel selection mode"
msgstr "선택 모드 끝내기"

#: data/resources/help-overlay.ui:83
msgctxt "shortcut window"
msgid "Select all"
msgstr "모두 선택"

# 글꼴, 색 설정 페이지
#: data/resources/help-overlay.ui:91
msgctxt "shortcut window"
msgid "Note edit mode"
msgstr "메모 편집 모드"

#: data/resources/help-overlay.ui:96
msgctxt "shortcut window"
msgid "Open in a new window"
msgstr "새 창에서 열기"

#: data/resources/help-overlay.ui:103
msgctxt "shortcut window"
msgid "Bold"
msgstr "굵게"

#: data/resources/help-overlay.ui:110
msgctxt "shortcut window"
msgid "Italic"
msgstr "기울임"

#: data/resources/help-overlay.ui:117
msgctxt "shortcut window"
msgid "Strike through"
msgstr "취소선"

#: data/resources/help-overlay.ui:124
msgctxt "shortcut window"
msgid "Undo"
msgstr "실행 취소"

#: data/resources/help-overlay.ui:131
msgctxt "shortcut window"
msgid "Redo"
msgstr "다시 실행"

#: data/resources/help-overlay.ui:138
msgctxt "shortcut window"
msgid "Move note to trash"
msgstr "휴지통으로 옮기기"

#: data/resources/import-dialog.ui:12
msgid "_Cancel"
msgstr "취소(_C)"

#: data/resources/import-dialog.ui:22
msgid "_Import"
msgstr "가져오기(_I)"

#: data/resources/import-dialog.ui:41
msgid "Select import location"
msgstr "가져오기 위치 선택"

#: data/resources/import-dialog.ui:78
msgid "Gnote application"
msgstr "지노트 프로그램"

#: data/resources/import-dialog.ui:136
msgid "Tomboy application"
msgstr "톰보이 프로그램"

#: data/resources/import-dialog.ui:192
msgid "Custom Location"
msgstr "사용자 설정 위치"

#: data/resources/import-dialog.ui:224
msgid "Select a Folder"
msgstr "폴더 선택"

#: data/resources/selection-toolbar.ui:43
msgid "Note color"
msgstr "메모 색"

#: data/resources/selection-toolbar.ui:60
msgid "Share note"
msgstr "메모 공유"

#: data/resources/selection-toolbar.ui:78
msgid "Open in another window"
msgstr "다른 창에서 열기"

#: data/resources/selection-toolbar.ui:119
msgid "Restore"
msgstr "복구"

#: data/resources/selection-toolbar.ui:127
msgid "Permanently Delete"
msgstr "완전히 삭제"

#: data/resources/settings-dialog.ui:37
msgid "Use System Font"
msgstr "시스템 글꼴 사용"

#: data/resources/settings-dialog.ui:50
msgid "Note Font"
msgstr "메모 글꼴"

#: data/resources/settings-dialog.ui:62
msgid "Default Color"
msgstr "기본 색"

#: data/resources/settings-dialog.ui:113
msgid "Note Appearance"
msgstr "메모 모양새"

#: data/resources/settings-dialog.ui:126
msgid "Select the default storage location:"
msgstr "기본 저장 경로 선택:"

# 메모 저장 위치 설정 페이지
#: data/resources/settings-dialog.ui:157
msgid "Primary Book"
msgstr "메모 저장"

#: src/bijiben-shell-search-provider.c:229 src/bjb-window.c:107
msgid "Untitled"
msgstr "제목 없음"

#: src/bjb-application.c:183
msgid "GNOME Notes"
msgstr "그놈 메모"

#: src/bjb-application.c:238
msgid "Show verbose logs"
msgstr "자세한 로그 표시"

#: src/bjb-application.c:240
msgid "Show the application’s version"
msgstr "프로그램의 버전을 표시합니다"

#: src/bjb-application.c:242
msgid "Create a new note"
msgstr "새 메모 만들기"

#: src/bjb-application.c:250
msgid "[FILE…]"
msgstr "[파일...]"

#: src/bjb-application.c:251
msgid "Take notes and export them everywhere."
msgstr "메모를 작성하고 내보냅니다."

#: src/bjb-application.c:590
msgid "Simple notebook for GNOME"
msgstr "그놈 데스크톱의 간단한 메모지"

#: src/bjb-application.c:596
msgid "translator-credits"
msgstr ""
"류창우 <cwryu@debian.org>\n"
"양홍민 <hongminy@naver.com>\n"
"조성호 <shcho@gnome.org>"

#: src/bjb-color-button.c:140
msgid "Note Color"
msgstr "메모 색"

#: src/bjb-note-list.c:108
msgid "Add Notes"
msgstr "메모 추가"

#: src/bjb-note-list.c:109
msgid "Use the + button to add a note"
msgstr "메모를 추가하려면 + 단추를 누르십시오"

#: src/bjb-note-list.c:114
msgid "No Results"
msgstr "결과 없음"

#: src/bjb-note-list.c:115
msgid "Try a different search"
msgstr "다른 단어로 검색해보십시오"

#: src/bjb-note-view.c:95
msgid "This note is being viewed in another window"
msgstr "이 메모는 다른 창에서 보고 있습니다"

#: src/bjb-utils.c:51
msgid "Unknown"
msgstr "알 수 없음"

#: src/bjb-utils.c:68
msgid "Yesterday"
msgstr "어제"

#: src/bjb-utils.c:74
msgid "This month"
msgstr "이번달"

#. Translators: %s is the note last recency description.
#. * Last updated is placed as in left to right language
#. * right to left languages might move %s
#. *         '%s Last Updated'
#.
#: src/bjb-window.c:750
#, c-format
msgid "Last updated: %s"
msgstr "최근 업데이트: %s"

#: src/libbiji/provider/biji-local-provider.c:363
msgid "Local storage"
msgstr "로컬 저장소"

#~ msgid "Notebook"
#~ msgstr "메모책"

#~ msgid "Oops"
#~ msgstr "앗"

#~ msgid "Please install “Tracker” then restart the application."
#~ msgstr "“트래커”를 설치하고 프로그램을 다시 시작하십시오."
